

class Function:
    def __init__(self, function_type, x_function, n_argument = None):

        self.function = x_function

        self.function_type = function_type

        function_types = {"pow": ["({argument} * [{function}] ^ [{argument} - 1] * {derivative})", "{function} ^ {argument}"],
                               "sqrt": ["([{derivative}] / [2 * sqrt({function})])", "sqrt({function})"],
                               "fract": ["(-[{derivative}] / [2 * sqrt({function})])", "fract([1] / [{function}])"],
                               "e-pow": ["([e]^[{function}] * {derivative})", "e ^ {function}"],
                               "log": ["([{derivative}] / [{function} * ln({argument})])", "log {argument}({function})"],
                               "ln": ["([{derivative}] / [{function}])", "ln({function})"],
                               "sin": ["(cos({function}) * {derivative})", "sin({function})"] ,
                               "cos": ["(- sin({function}) * {derivative})", "cos({function})"],
                               "tg": ["([{derivative}] / [cos^2({function})])", "tg({function})"],
                               "ctg": ["(-[{derivative}] / [sin^2({function})])", "ctg({function})"]}
        self.derivative = function_types[self.function_type][0]
        self.function_viewer = function_types[self.function_type][1]


        self.n_argument = n_argument

    def showfunction(self):
        if self.function != type(Function):
            return str(self.function_viewer.format(function = self.function, argument = self.n_argument))
        else:
            return str(self.function.showfunction())

    def showderivative_function(self):
         if self.function != type(Function):
             return str(self.derivative.format(derivative = self.function.derivative.format(derivative = 1, argument = self.n_argument, function = self.function.function), argument = self.n_argument, function = self.function.showfunction()))
         else:
            return str(self.derivative.format(derivative = self.function.showderivative_function(), function = self.function.showfunction(), argument = self.n_argument))

