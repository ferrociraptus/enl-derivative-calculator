from interface import Ui_window
from PyQt5 import QtWidgets
from sympy import *
from LatexVisualisation_Qt import Widget
# from PyQt5.QtCore import Qt

import sys


class My_window(QtWidgets.QMainWindow):
    def __init__(self):
        super(My_window, self).__init__()
        self.ui = Ui_window()
        self.ui.setupUi(self)
        self.args = []
        # self.buff = ''
        from PyQt5.QtCore import Qt
        # self.ui.input_line.setCursorMoveStyle(Qt.LogicalMoveStyle)


        # buttons
        self.ui.pow_button.clicked.connect(self.pow_add)
        self.ui.multiplication_button.clicked.connect(self.multiplication_add)
        self.ui.division_button.clicked.connect(self.division_add)
        self.ui.summation_button.clicked.connect(self.summation_add)
        self.ui.subtraction_button.clicked.connect(self.subtraction_add)
        self.ui.sin_button.clicked.connect(self.sin_add)
        self.ui.cos_button.clicked.connect(self.cos_add)
        self.ui.tg_button.clicked.connect(self.tg_add)
        self.ui.ctg_button.clicked.connect(self.ctg_add)
        self.ui.sqrt_button.clicked.connect(self.sqrt_add)
        self.ui.cbrt_button.clicked.connect(self.cbrt_add)
        self.ui.arcsin_button.clicked.connect(self.arcsin_add)
        self.ui.arccos_button.clicked.connect(self.arccos_add)
        self.ui.arctg_button.clicked.connect(self.arctg_add)
        self.ui.arcctg_button.clicked.connect(self.arcctg_add)
        self.ui.log_button.clicked.connect(self.log_add)
        self.ui.ln_button.clicked.connect(self.ln_add)
        self.ui.lg_button.clicked.connect(self.lg_add)
        self.ui.argument_button.clicked.connect(self.arg_add)
        self.ui.new_arguments_line_edit.editingFinished.connect(self.new_arg)
        self.ui.clean_input_window.clicked.connect(self.clean_input)
        self.ui.show_derivative_function_button.clicked.connect(self.show_derivative)
        self.ui.input_line.cursorPositionChanged.connect(self.no_err)
        self.ui.cosec_button.clicked.connect(self.cosec_add)
        self.ui.arccosec_button.clicked.connect(self.arccosec_add)
        self.ui.sec_button.clicked.connect(self.sec_add)
        self.ui.arcsec_button.clicked.connect(self.arcsec_add)
        self.ui.derivative_depth_combobox.addItems(str(i) for i in range(1, 11))
        self.ui.derivative_depth_combobox.setCurrentIndex(0)
        self.ui.sinc_button.clicked.connect(self.sinc_add)

    def no_err(self):
         self.ui.error_label.clear()
         self.ui.input_line.setStyleSheet("background-color: gray;")

    def show_derivative(self):
        x = symbols("x")
        function = self.formatin_input(self.ui.input_line.displayText())
        try:
            for i in range(0, int(self.ui.derivative_depth_combobox.currentText())):
                function = diff(function, x)

            # if self.buff != self.ui.input_line.displayText():
            #     self.buff = self.ui.input_line.displayText()
            latex_output = latex(function)
            # a = QApplication(*argv)
            w = Widget(latex_output)
            w.show()
            w.raise_()
                # exit(a.exec_())

        except:
            self.ui.error_label.setText("<font color = red>Error: incorrect input</font>")
            self.ui.input_line.setStyleSheet("background-color: rgb(255, 80, 80);")

    def clean_input(self):
        self.ui.input_line.clear()

    def new_arg(self):
        self.args = symbols((((self.ui.new_arguments_line_edit.displayText()
                               .strip()).replace(',', '')).replace(';', '')).split())

    def sinc_add(self):
        self.ui.input_line.insert("sinc()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def sec_add(self):
        self.ui.input_line.insert("sec()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arcsec_add(self):
        self.ui.input_line.insert("arcsec()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def cosec_add(self):
        self.ui.input_line.insert("cosec()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arccosec_add(self):
        self.ui.input_line.insert("arccosec()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arg_add(self):
        self.ui.input_line.insert("x")
        self.ui.input_line.setCursorPosition(len(self.ui.input_line.displayText()))

    def lg_add(self):
        self.ui.input_line.insert("log( , 10)")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 5)

    def ln_add(self):
        self.ui.input_line.insert("ln()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def log_add(self):
        self.ui.input_line.insert("log( , )")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 3)

    def arcsin_add(self):
        self.ui.input_line.insert("arcsin()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arccos_add(self):
        self.ui.input_line.insert("arccos()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arctg_add(self):
        self.ui.input_line.insert("arctg()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def arcctg_add(self):
        self.ui.input_line.insert("arcctg()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def sqrt_add(self):
        self.ui.input_line.insert("sqrt()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def cbrt_add(self):
        self.ui.input_line.insert("cbrt()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def ctg_add(self):
        self.ui.input_line.insert("ctg()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def tg_add(self):
        self.ui.input_line.insert("tg()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def cos_add(self):
        self.ui.input_line.insert("cos()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def sin_add(self):
        self.ui.input_line.insert("sin()")
        pos = self.ui.input_line.cursorPosition()
        self.ui.input_line.setCursorPosition(pos - 1)

    def subtraction_add(self):
        self.ui.input_line.insert(" - ")

    def summation_add(self):
        self.ui.input_line.insert(" + ")

    def division_add(self):
        self.ui.input_line.insert(" / ")

    def multiplication_add(self):
        self.ui.input_line.insert(" * ")

    def pow_add(self):
        self.ui.input_line.insert(" ^ ")
        # pos = self.ui.input_line.cursorPosition()
        # self.ui.input_line.setCursorPosition(pos - 3)

    def formatin_input(self, text):
        text = text.replace('arccosec', 'acsc')
        text = text.replace('arcsec', 'asec')
        text = text.replace('arcctg', 'acot')
        text = text.replace('arcsin', 'asin')
        text = text.replace('arccos', 'acos')
        text = text.replace('arctg', 'atan')
        text = text.replace('**', '^')
        text = text.replace('cosec', 'csc')
        text = text.replace('ctg', 'cot')
        text = text.replace('tg', 'tan')
        return text
    #
    # def formatin_output(self, text):
    #     text = text.replace('acsc', 'arccosec')
    #     text = text.replace('asec', 'arcsec')`
    #     text = text.replace('acot', 'arcctg')
    #     text = text.replace('asin', 'arcsin')
    #     text = text.replace('acos', 'arccos')
    #     text = text.replace('atan', 'arctg')
    #     text = text.replace('**', '^')
    #     text = text.replace('csc', 'cosec')
    #     text = text.replace('tan', 'tg')
    #     text = text.replace('cot', 'ctg')
    #     return text




if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    application = My_window()
    application.show()
    sys.exit(app.exec())